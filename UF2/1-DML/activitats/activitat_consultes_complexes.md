# Activitat: Consultes complexes

## Base de dades

Per aquests exercicis utilitzarem la base de dades _Chinook_.

Totes les qüestions s'han de resoldre utilitzant una única consulta.

## Consultes

1. Cerca quants àlbums hi ha de Deep Purple.

2. Mostra el nom i cognoms dels empleats que estat assignats com a suport
d'algun client (_SupportRepId_).

3. Mostra el nom i cognoms de __tots__ els empleats i la quantitat de clients
a qui estan assignats per donar suport.

4. Troba el nom de totes les cançons de l'àlbum _Back to Black_ i el nom de
l'artista que l'ha fet.

5. Troba els beneficis que hem obtingut amb el gènere _Blues_.

6. Troba els diners que hem fet per cadascun dels gèneres musicals.

7. Troba els beneficis que hem obtinguts per cadascun dels gèneres muscials.
Volem una llista amb __tots__ els gèneres, encara que no s'hagi venut cap cançó
d'un gènere determinat.

8. Troba els beneficis que hem obtingut amb els gèneres pels quals hem obtingut
més de 100 dólars. Ordena els resultats pels beneficis obtinguts.

9. Troba el nom de tots els artistes que es diuen igual que alguna de les
cançons.

10. Volem una llista del nom i cognoms de __tots__ els clients i de quantes
comandes han fet cadascun.

11. Obté una llista de tots els gèneres de la base de dades i de quantes
cançons hi ha de cada gènere.

12. Cerca el nom dels tres grups dels quals hi ha més àlbums.

13. Cerca el nom dels grups dels quals hi ha més de 8 àlbums.

14. Cerca els 5 àlbums que tenen una durada mitjana de les seves pistes més gran.
De cada àlbum en volem el seu nom i el nom del grup.

15. Cerca el nom dels tres gèneres dels quals en tenim més pistes.

16. Obté el número i el nom de les llistes (_Playlist_) que contenen més de mil
cançons.

17. Cerca els noms de les cançons que ens han reportat més de 2 dólars de
beneficis.

18. Troba el nom de totes les cançons, el nom de l'àlbum, i el nom de l'artista,
que s'han comprat a la factura 225.

19. Volem una llista dels noms de les cançons i de l'àlbum al qual pertanyen de
totes les cançons que apareixen a més de quatre llistes de reproducció.

20. Mostra la llista de reproducció anomenada _Grunge_. Volem veure una llista
amb totes les seves cançons, amb el títol de la cançó, la seva durada en segons
i el nom de l'artista.

21. Mostra el nom dels artistes que han creat més de 5 cançons del gènere
_World_.

22. Troba el nom de les cançons que ha comprat el client Enrique Muñoz, i la
data en què s'ha comprat cadascuna de les cançons.

23. Troba l'artista preferit de l'Enrique Muñoz (l'artista del qual ha comprat
més cançons).

24. Obté una llista del nom i cognom de __tots__ els empleats juntament amb el
nom i cognom de l'empleat de qui depenen (_ReportsTo_).

25. Volem la mateixa llista que a l'exercici anterior, però al revés: per a
cada empleat, volem veure quins empleats depenen d'ell. Com abans, volem una
llista amb tots els empleats.

26. Volem la mateixa llista d'empleats que a l'exercici anterior, però només
el nom i cognoms de cada empleat i la quantitat d'empleats que depenen d'ell.

27. Troba el nom dels artistes que coincideix amb el nom d'alguna cançó d'un
altre artista. Volem veure el nom dels dos artistes relacionats.

28. Troba el nom dels artistes que coincideix amb part del nom d'alguna cançó
d'un altre artista. Volem veure el nom dels dos artistes relacionats i el títol
de la cançó. _Pista_: Utilitza la funció CONCAT per concatenar diverses
cadenes de text.
