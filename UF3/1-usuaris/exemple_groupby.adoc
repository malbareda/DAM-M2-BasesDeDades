= Group by en PostgreSQL i MySQL
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

----
postgres=# create table t1 (a serial primary key, b int);
CREATE TABLE
postgres=# create table t2 (c serial primary key, d int, fa int references t1(a));
CREATE TABLE
postgres=# insert into t1(b) values (1),(2),(3);
INSERT 0 3
postgres=# insert into t2(d,fa) values (1,1),(2,1),(3,2),(4,2),(5,3),(6,3);
INSERT 0 6
postgres=# select * from t1;
 a | b
---+---
 1 | 1
 2 | 2
 3 | 3
(3 rows)

postgres=# select * from t2;
 c | d | fa
---+---+----
 1 | 1 |  1
 2 | 2 |  1
 3 | 3 |  2
 4 | 4 |  2
 5 | 5 |  3
 6 | 6 |  3
(6 rows)

postgres=# select a,b,c,d,fa from t1 join t2 on t1.a=t2.fa group by a;
ERROR:  column "t2.c" must appear in the GROUP BY clause or be used in an aggregate function
LINE 1: select a,b,c,d,fa from t1 join t2 on t1.a=t2.fa group by a;
                   ^
postgres=# select a,b,c,d,fa from t1 join t2 on t1.a=t2.fa group by a,c;
 a | b | c | d | fa
---+---+---+---+----
 1 | 1 | 1 | 1 |  1
 3 | 3 | 5 | 5 |  3
 1 | 1 | 2 | 2 |  1
 3 | 3 | 6 | 6 |  3
 2 | 2 | 3 | 3 |  2
 2 | 2 | 4 | 4 |  2
(6 rows)
----

----
MariaDB [groupbytest]> create table t1 (a int unsigned auto_increment primary key, b int);
Query OK, 0 rows affected (0.26 sec)

MariaDB [groupbytest]> create table t2 (c int unsigned auto_increment primary key, d int, fa int unsigned references t1(a));
Query OK, 0 rows affected (0.28 sec)

MariaDB [groupbytest]> insert into t1(b) values (1),(2),(3);
Query OK, 3 rows affected (0.04 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [groupbytest]> insert into t2(d,fa) values (1,1),(2,1),(3,2),(4,2),(5,3),(6,3);
Query OK, 6 rows affected (0.05 sec)
Records: 6  Duplicates: 0  Warnings: 0

MariaDB [groupbytest]> select * from t1;
+---+------+
| a | b    |
+---+------+
| 1 |    1 |
| 2 |    2 |
| 3 |    3 |
+---+------+
3 rows in set (0.00 sec)

MariaDB [groupbytest]> select * from t2;
+---+------+------+
| c | d    | fa   |
+---+------+------+
| 1 |    1 |    1 |
| 2 |    2 |    1 |
| 3 |    3 |    2 |
| 4 |    4 |    2 |
| 5 |    5 |    3 |
| 6 |    6 |    3 |
+---+------+------+
6 rows in set (0.00 sec)

MariaDB [groupbytest]> select a,b,c,d,fa from t1 join t2 on t1.a=t2.fa group by a;
+---+------+---+------+------+
| a | b    | c | d    | fa   |
+---+------+---+------+------+
| 1 |    1 | 1 |    1 |    1 |
| 2 |    2 | 3 |    3 |    2 |
| 3 |    3 | 5 |    5 |    3 |
+---+------+---+------+------+
3 rows in set (0.00 sec)

MariaDB [groupbytest]> select a,b,c,d,fa from t1 join t2 on t1.a=t2.fa group by a,c;
+---+------+---+------+------+
| a | b    | c | d    | fa   |
+---+------+---+------+------+
| 1 |    1 | 1 |    1 |    1 |
| 1 |    1 | 2 |    2 |    1 |
| 2 |    2 | 3 |    3 |    2 |
| 2 |    2 | 4 |    4 |    2 |
| 3 |    3 | 5 |    5 |    3 |
| 3 |    3 | 6 |    6 |    3 |
+---+------+---+------+------+
6 rows in set (0.00 sec)
----
