= Bases de dades per a desenvolupadors
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

Materials per al mòdul *M2 - Bases de dades* del cicle de
*Desenvolupament d'Aplicacions Multiplataforma*.

[link="http://creativecommons.org/licenses/by-sa/4.0/"]
image::https://i.creativecommons.org/l/by-sa/4.0/88x31.png[Creative Commons License]

This work is licensed under a
link:http://creativecommons.org/licenses/by-sa/4.0/[Creative Commons Attribution-ShareAlike 4.0 International License].

== Índex

link:attribution.md[Atribucions]

=== UF 1: introducció a les bases de dades

1. link:UF1/1-Introduccio_a_les_BD[Introducció a les bases de dades]
2. Model entitat-relació
+
--
- link:UF1/2-model_ER/model_er.adoc[Teoria]

Activitats:

- link:UF1/2-model_ER/activitats/aules_informatica.adoc[Aules d'informàtica]
- link:UF1/2-model_ER/activitats/activitats_extraescolars.adoc[Activitats extraescolars]
- link:UF1/2-model_ER/activitats/temperatures.adoc[Dades meteorològiques]
- link:UF1/2-model_ER/activitats/cinefil.adoc[Cinèfil]
- link:UF1/2-model_ER/activitats/concessionari.adoc[Concessionari]
- link:UF1/2-model_ER/activitats/fabricant.adoc[Fabricant]
- link:UF1/2-model_ER/activitats/eleccions.adoc[Eleccions]
- link:UF1/2-model_ER/activitats/protectora_animals.adoc[Protectora animals]
- link:UF1/2-model_ER/activitats/estacio_espacial_estructura.adoc[Estació espacial - estructura i passatgers]
- link:UF1/2-model_ER/activitats/xarxa_social.adoc[Xarxa social]
- link:UF1/2-model_ER/activitats/estacio_espacial_duana.adoc[Estació espacial - duana]
- link:UF1/2-model_ER/activitats/festival.adoc[Festival de música]
- link:UF1/2-model_ER/activitats/ritmica.adoc[Campionat de gimnàstica rítmica]
--

3. link:UF1/3-model_relacional[Model relacional]
+
--
- link:UF1/3-model_relacional/relacional.adoc[Teoria]

Activitats:

- link:UF1/2-model_ER/solucions/aules_informatica.png[Aules d'informàtica]
- link:UF1/2-model_ER/solucions/activitats_extraescolars.png[Activitats extraescolars]
- link:UF1/2-model_ER/solucions/temperatures.png[Dades meteorològiques]
- link:UF1/2-model_ER/solucions/concessionari.png[Concessionari]
- link:UF1/2-model_ER/solucions/eleccions.png[Eleccions]
- link:UF1/2-model_ER/solucions/protectora.png[Protectora animals]
--

4. link:UF1/4-normalitzacio[Normalització]

=== UF 2: llenguatges SQL: DML i DDL

1. Llenguatges de base de dades per manipular dades
+
--
 - link:UF2/1-DML/dml.adoc[Teoria]

Activitats:

 - link:UF2/1-DML/activitats/activitat_primeres_passes_mariadb.adoc[Primeres passes en MariaDB]
 - link:UF2/1-DML/activitats/activitat_consultes_simples.adoc[Consultes simples]
 - link:UF2/1-DML/activitats/activitat_mes_consultes_simples.md[Més consultes simples]
 - link:UF2/1-DML/activitats/activitat_consultes_agregacio.md[Consultes agregació]
 - link:UF2/1-DML/activitats/activitat_consultes_complexes.md[Consultes complexes]
 - link:UF2/1-DML/activitats/activitat_funcions_integrades.md[Funcions integrades]
 - link:UF2/1-DML/activitats/activitat_subconsultes.md[Subconsultes]
 - link:UF2/1-DML/activitats/activitat_modificacio.md[Modificació de dades]
--

2. Llenguatges de la base de dades per crear l’estructura de la base de dades
+
--
  - link:UF2/2-DDL[Teoria]

Activitats:

  - link:UF2/2-DDL/activitats/activitat_tipus_de_dades.md[Tipus de dades]
  - link:UF2/2-DDL/activitats/creacio_taules.adoc[Creació de taules]
--

3. link:UF2/3-transaccions[Estratègies per al control de les transaccions i la concurrència]

=== UF 3: llenguatges SQL: DCL i extensió procedimental

1. Gestió d'usuaris
+
--
  - link:UF3/1-usuaris/usuaris.adoc[Teoria]

Activitats:

  - link:UF3/1-usuaris/exemple_groupby.adoc[Exemple GROUP BY en Postgresql]
  - link:UF3/1-usuaris/activitat_usuaris.adoc[Activitat usuaris]
--

2. Programació en bases de dades
+
--
  - link:UF3/2-programacio/programacio.adoc[Teoria]

Activitats:

  - link:UF3/2-programacio/activitats/activitat_funcions_sql.adoc[Activitat funcions SQL]
  - link:UF3/2-programacio/activitats/activitat_funcions_plpgsql.adoc[Activitat funcions PL/pgSQL]
  - link:UF3/2-programacio/activitats/activitat_funcions_plpgsql2.adoc[Activitat funcions PL/pgSQL 2]
  - link:UF3/2-programacio/activitats/activitat_cursors.adoc[Activitat cursors]
  - link:UF3/2-programacio/activitats/activitat_triggers.adoc[Activitat disparadors]
--

=== UF 4: bases de dades objecte-relacionals

1. Ús de bases de dades objecte-relacionals
+
--
  - link:UF4/1-mongodb/mongodb.adoc[Teoria]

Activitats:

  - link:UF4/1-mongodb/activitats/activitat_json.adoc[Activitat JSON]
  - link:UF4/1-mongodb/activitats/activitat_consultes1.adoc[Activitat consultes 1]
  - link:UF4/1-mongodb/activitats/activitat_consultes2.adoc[Activitat consultes 2]
  - link:UF4/1-mongodb/activitats/activitat_consultes3.adoc[Activitat consultes 3]
  - link:UF4/1-mongodb/activitats/activitat_aggregation.adoc[Activitat aggregation framework]
--
